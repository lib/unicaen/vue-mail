# Unicaen-vue-mail

Cette bibliothèques à pour but de fournir un front Vue à la bibliothèques [unicaen-mail >=7.0.0](https://git.unicaen.fr/lib/unicaen/mail).


## Install

1. Ajouter le registry npm d'unicaen : https://kverdaccio.unicaen.fr/

2. Assurez vous d'avoir Pinia et Vuetify d'instancié: [exemple](INSTANCIATION.md)

3. Utiliser le composant Mail
   ```js
      <script setup>
      import Mail from 'unicaen-vue-mail';
      </script>
   ```
   ```js
      <template>
         <Mail />
      </template>
   ```

## Outils associés

- [Vuetify](https://vuetifyjs.com/)
- [Pinia](https://pinia.vuejs.org/)
- [Vite](https://vitejs.dev/)
- [Typescript](https://www.typescriptlang.org/)

## Dev

#### Starting the Development Server

To start the development server with hot-reload, run the following command. The server will be accessible at [http://localhost:3000](http://localhost:3000):

```bash
npm run dev
```

#### Building for Production

To build your project for production, use:

```bash
npm run build
```

Once the build process is completed, your application will be ready for deployment in a production environment.

