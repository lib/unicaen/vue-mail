## Pinia
Instancier Pinia
```bash
npm install pinia
```
```js
import { createPinia } from 'pinia'
const pinia = createPinia();
app.use(pinia);
```
## Vuetify
Instancier Vuetify (+fontawesome)

```bash
npm install vuetify @fortawesome/vue-fontawesome @fortawesome/fontawesome-svg-core @fortawesome/free-solid-svg-icons @fortawesome/free-regular-svg-icons
```

```js
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from "vuetify/components";
import * as directives from "vuetify/directives";
import { aliases, fa } from 'vuetify/iconsets/fa-svg'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(fas) // Include solid icons
library.add(far) // Include regular icons

const vuetify = createVuetify({
    components,
    directives,
    icons: {
        defaultSet: 'fa',
        aliases,
        sets: {
            fa,
        },
    },
    defaults: {
        VBtn: {
            color: 'primary',
        },
    }
})
app.component('font-awesome-icon', FontAwesomeIcon)
app.use(pinia)
```