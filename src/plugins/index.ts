/**
 * plugins/index.ts
 *
 * Automatically included in `./src/main.ts`
 */

// Plugins
import vuetify from './vuetify'
import pinia from './pinia'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

// Types
import type { App } from 'vue'

export function registerPlugins (app: App) {
  app.use(vuetify)
  app.use(pinia)
  app.component('font-awesome-icon', FontAwesomeIcon) // Register component globally
}
