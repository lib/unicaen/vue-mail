import MockAdapter from "axios-mock-adapter";
import mockData from "@/MOCK_DATA.json";
import moment from "moment";

import type { AxiosInstance } from "axios";
import type { Mail } from "@/types";

export default {
  mocked(axiosClient: AxiosInstance) {
    console.log('Mocked')
    const mock = new MockAdapter(axiosClient, { delayResponse: 1000 });

    mock.onGet("/mail-api").reply((config) => {
        return [200, mockData];
    });

    mock.onPost("/mail-api/test").reply((config) => {
        const data: FormData = config.data
        const mailTo = data.get('mail-to') as string
        const mail: Mail = {
            id: Math.floor(Math.random() * 1000),
            destinataires: mailTo,
            destinatairesInitials: null,
            sujet: 'Test',
            corps: 'Test',
            dateEnvoi: moment().unix(),
            statusEnvoi: 'SUCCESS',
            copies: null,
            log: null,
            motsClefs: "TEST"
        }
        return [200, mail];
    });

    mock.onPost("/mail-api/reenvoi").reply((config) => {
        const mail: Mail = {
            id: Math.floor(Math.random() * 1000),
            destinataires: "mailTo@resent.com",
            destinatairesInitials: null,
            sujet: 'Resent',
            corps: 'Test',
            dateEnvoi: moment().unix(),
            statusEnvoi: 'SUCCESS',
            copies: null,
            log: null,
            motsClefs: "RESENT"
        }
        return [200, mail];
    });

    const deleteUrl = "/mail-api";
    mock.onDelete(new RegExp(`${deleteUrl}/*`)).reply((config) => {
      return [200];
  });
  }
};

