import axios from 'axios'
import Mock from './mock'

const axiosClient = axios.create({
    baseURL: import.meta.env.BASE_URL
})
if (import.meta.env.DEV) {
    Mock.mocked(axios)
}
export default { axiosClient }