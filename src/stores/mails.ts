import { defineStore } from 'pinia'
import { ref } from 'vue'
import type { Mail } from '@/types'
import moment from 'moment'
import axios from 'axios'
import Mock from '@/plugins/mock'

if (import.meta.env.DEV) {
    Mock.mocked(axios)
}

export const useMailsStore = defineStore('mails', {
    state: () => (
        {
            mails: ref<Mail[]>([]),
            motsClefsSeparateur: ref<string>('||'),
            loading: ref<boolean>(false),
            error: ref<string | null>(null),
            message: ref<string | null>(null),
            alert: ref<boolean>(false),
        }
    ),
    getters: {
        getMotsClefsUnique: (state): string[] => {
            let allMotsClefs: string[] = state.mails.flatMap(mail => mail.motsClefs ? mail.motsClefs.split(state.motsClefsSeparateur) : []);
            allMotsClefs = Array.from(new Set(allMotsClefs));
            // add empty string to the list
            allMotsClefs.unshift('Tous');
            return allMotsClefs;
        }
    },
    actions: {
        filterMails(filterHistory: number, filterStatus: string[], filterMotsClefs: string): Mail[] {
            return this.mails.filter(mail => {
                const mailDate = moment.unix(mail.dateEnvoi);
                const now = moment();
                const diffMonths = now.diff(mailDate, 'months', true);
                const matchHistory = (filterHistory > 0) ? diffMonths <= filterHistory : true;
        
                const matchStatus = (filterStatus.length) ? filterStatus.includes(mail.statusEnvoi) : true;
                const matchMotsClefs = (filterMotsClefs !== 'Tous') 
                ? mail.motsClefs?.split(this.motsClefsSeparateur).includes(filterMotsClefs)
                : true;
        
                return matchHistory && matchStatus && matchMotsClefs;
            })
        },
        async fetchMails() {
            this.loading = true
            try {
                const response = await axios.get('/mail-api')
                this.mails = response.data
            } catch (error: any) {
                this.error = error.message || error
            } finally {
                this.loading = false
                this.alert = true
            }
        },
        async deleteMail(id: number) {
            this.loading = true
            try {
                await axios.delete('/mail-api/'+id)
                this.mails = this.mails.filter(mail => mail.id !== id)
                this.message = `Mail ${id} supprimé`
            } catch (error: any) {
                console.log(error)
                this.error = error.message
            } finally {
                this.loading = false
                this.alert = true
            }
        },
        async reenvoiMail(id: number) {
            this.loading = true
            try {
                const response = await axios.post('/mail-api/reenvoi',
                { 
                    id: id
                }, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                })
                this.mails.unshift(response.data)
                this.message = `Mail ${id} réenvoyé`
            } catch (error: any) {
                this.error = error.message
            } finally {
                this.loading = false
                this.alert = true
            }
        },
        async testMail(to: string) {
            this.loading = true
            try {
                const response = await axios.post('/mail-api/test', {
                    'mail-to': to
                }, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                })
                this.mails.unshift(response.data)
                this.message = `Mail de test envoyé`
            } catch (error: any) {
                this.error = error.message
            } finally {
                this.loading = false
                this.alert = true
            }
        }
    },
})