// types.ts
export interface Mail {
    id: number;
    destinataires: string | null;
    dateEnvoi: number;
    copies: string | null;
    statusEnvoi: string;
    sujet: string | null;
    corps: string | null;
    log: string | null;
    motsClefs: string | null;
    destinatairesInitials: string | null;
  }
  
export interface Props {
    title?: string,
    mails?: Mail[],
    params?: any[],
    loading?: boolean,
    error?: string | null,
}

export interface Options {
    motsClefsSeparateur: string
}